package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Project;

@NoArgsConstructor
public final class ProjectCascadeRemoveResponse extends AbstractProjectResponse {

    public ProjectCascadeRemoveResponse(@Nullable Project project) {
        super(project);
    }

}
