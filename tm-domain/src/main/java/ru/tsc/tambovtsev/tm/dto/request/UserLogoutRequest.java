package ru.tsc.tambovtsev.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class UserLogoutRequest extends AbstractUserRequest{

    public UserLogoutRequest(@Nullable String token) {
        super(token);
    }

}
